<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                font-family: 'Nunito', sans-serif;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .position-ref {
                position: relative;
            }

            .ml {
                margin-left: 20px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height ml">
            <h1>SanberBook</h1>
            <h2>Sosial Media Developer Santai Berkualitas</h2>
            <p>Belajar dan berbagi agar hidup ini semakin santai berkualitas</p>

            <h3>Benefit Join di SanberBook</h3>
            <ul>
                <li>Mendapatkan motivasi dari sesama developer</li>
                <li>Sharing knowledge dari para mastah Sanber</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>

            <h3>Cara Bergabung ke SanberBook</h3>
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftar di <a href="{{ url('/register') }}">Form Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
        </div>
    </body>
</html>
