<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Form Registrasi</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                font-family: 'Nunito', sans-serif;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .position-ref {
                position: relative;
            }

            .ml {
                margin-left: 20px;
            }

            .mb {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height ml">
            <h1>Buat Account Baru!</h1>
            <h3>Sign Up Form</h3>
            
            <form action="/welcome" method="post">
                @csrf

                <label>Fisrt name :</label><br>
                <input type="text" name="fisrtname"><br>
                <br>
                <label>Last name :</label><br>
                <input type="text" name="lastname"><br>
                <br>
                <label>Gender :</label><br>
                <input type="radio" name="" value="Male"><label>Male</label><br>
                <input type="radio" name="" value="Female"><label>Female</label><br>
                <input type="radio" name="" value="Other"><label>Other</label><br>
                <br>
                <label>Nationality :</label><br>
                <select>
                    <option selected>Indonesia</option>
                    <option>Amerika</option>
                    <option>Inggris</option>
                </select>
                <br><br>
                <label>Language Spoken :</label><br>
                <input type="checkbox" name="" value="Bahasa Indonesia"><label>Bahasa Indonesia</label><br>
                <input type="checkbox" name="" value="English"><label>English</label><br>
                <input type="checkbox" name="" value="Arabic"><label>Arabic</label><br>
                <input type="checkbox" name="" value="Japanese"><label>Japanese</label><br>
                <br>
                <label>Bio :</label><br>
                <textarea rows="8" cols="30"></textarea><br><br>
                <input type="submit" value="Sign Up" class="mb">
            </form>
        </div>
    </body>
</html>
