@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('judulbox')
Sign Up Form
@endsection

@section('content')        
    <form action="/welcome" method="post">
        @csrf

        <div class="card-body">
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Fisrt name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Fisrt name" name="fisrtname">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Last name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Last name" name="lastname">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Gendr</label>
                <div class="col-sm-10">
                    <div class="form-check">
                      <input class="form-check-input" type="radio">
                      <label class="form-check-label">Male</label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio">
                      <label class="form-check-label">Female</label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio">
                      <label class="form-check-label">Other</label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Nationality</label>
                <div class="col-sm-10">
                    <select class="form-control">
                        <option selected>Indonesia</option>
                        <option>Amerika</option>
                        <option>Inggris</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Language Spoken</label>
                <div class="col-sm-10">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="Bahasa Indonesia">
                      <label class="form-check-label">Bahasa Indonesia</label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="English">
                      <label class="form-check-label">English</label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="Arabic">
                      <label class="form-check-label">Arabic</label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="Japanese">
                      <label class="form-check-label">Japanese</label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Biodata</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                </div>
            </div>
        </div>

        <input type="submit" value="Sign Up" class="btn btn-info">
    </form>
@endsection
