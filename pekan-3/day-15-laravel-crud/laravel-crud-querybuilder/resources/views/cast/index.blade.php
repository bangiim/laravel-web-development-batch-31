@extends('layout.master')

@section('judul')
Data Cast
@endsection

@section('judulbox')
Data Cast All Film
@endsection

@section('content')
  <a href="/cast/create" class="btn btn-primary mb-4"><i class="fas fa-plus"></i> Add Data</a>
  <div class="card">
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($cast as $key => $item)
            <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama}}</td>
              <td>{{$item->umur}}</td>
              <td>{{$item->bio}}</td>
              <td>
                <form action="/cast/{{$item->id}}" method="post">
                  <a href="/cast/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
                  <a href="/cast/edit/{{$item->id}}" class="btn btn-warning btn-sm">Edit</a>
                  
                  @csrf
                  @method('delete')
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
                {{-- <a href="/cast/create" class="btn btn-danger btn-sm">Delete</a> --}}
              </td>
            </tr>

          @empty
            <tr>
              <td>Data Masih Kosong</td>
            </tr>

          @endforelse
        </tbody>
        <tfoot>
        <tr>
          <th>#</th>
          <th>Nama</th>
          <th>Umur</th>
          <th>Bio</th>
          <th>Action</th>
        </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection

@push('script')
  <!-- DataTables -->
  <script src="{{asset('AdminLTE-3.1.0/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('AdminLTE-3.1.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush

@push('style')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.1.0/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('AdminLTE-3.1.0/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('AdminLTE-3.1.0/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endpush
