@extends('layout.master')

@section('judul')
Cast
@endsection

@section('judulbox')
Cast Edit ID : {{$cast->id}}
@endsection

@section('content')        
    <form action="/cast/{{ $cast->id }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label class="col-form-label">Nama</label>
            <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{$cast->nama}}">
            @error('nama')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label class="col-form-label">Umur</label>
            <input type="text" class="form-control" placeholder="Umur" name="umur" value="{{$cast->umur}}">
            @error('umur')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label class="col-form-label">Biodata</label>
            <textarea class="form-control" rows="3" placeholder="Enter Bio ..." name="bio">{{$cast->bio}}</textarea>
            @error('bio')
                <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <input type="submit" value="Submit" class="btn btn-primary">
        <a href="/cast" class="btn btn-danger">Cancel</a>
    </form>
@endsection
