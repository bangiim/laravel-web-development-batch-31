@extends('layout.master')

@section('judul')
Detail Data Cast
@endsection

@section('judulbox')
Detail Data Cast : {{$cast->nama}}
@endsection

@section('content')
    <table class="table table-bordered">
      <tbody>
        <tr>
          <th style="width: 20%">#</th>
          <td>{{$cast->id}}</td>
        </tr>
        <tr>  
          <th style="width: 20%">Nama</th>
          <td>{{$cast->nama}}</td>
        </tr>
        <tr>  
          <th style="width: 20%">Umur</th>
          <td>{{$cast->umur}}</td>
        </tr>
        <tr>
          <th style="width: 20%">Biodata</th>
          <td>{{$cast->bio}}</td>
        </tr>
      </tbody>
    </table>

    <a href="/cast" class="btn btn-info mt-4">Back</a>
    <a href="/cast/edit/{{$cast->id}}" class="btn btn-warning mt-4">Edit</a>
@endsection